require 'yaml'

module DeliveryCharges



  def calc_delivery_charges(subtotal)
    charge = charges.find { |rule|
      c = rule['condition']
      if c.size > 1
        c['lt'] > subtotal && c['gt'] < subtotal
      else
        (c.has_key?('lt') && c['lt'] > subtotal) ||
        (c.has_key?('gt') && c['gt'] < subtotal)
      end
    }
    charge['value']
  end

  private

    def charges
      @charges ||= YAML::load_file('config/seeds/delivery_charge.yml')['delivery_charge']
    end
end
