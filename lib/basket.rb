require "bigdecimal"
require "virtus"
require "delivery_charges"
require "offers"

class Basket
  include Virtus.model
  include DeliveryCharges
  include Offers

  attribute :catalog, Hash
  attribute :basket, Array

  BasketEntry = Struct.new(:product_id, :description, :price) do
    def to_s
      "%s\t£%d" % [description, price] 
    end
  end

  def add(item)
    product = catalog.fetch(item) 
    add_basket_entry(item, product)

    if (discount = calc_discount(item)).nonzero?
      add_basket_entry(nil, {
          description: "Discount #{'%d' % (discount * 100)}% #{product[:description]}",
          price: product[:price] * discount
        })
    end
  end

  def subtotal
    @_subtotal ||= begin
      basket.inject(0) { |mem, prod|
        mem += BigDecimal(prod['price'].to_s)
        }.floor(2)
    end
  end

  def total
    if (delivery = calc_delivery_charges(subtotal)).nonzero?
      add_basket_entry(nil, {description: 'Delivery Charges', price: delivery})
      subtotal + delivery
    else
      subtotal
    end
  end

  private

    def add_basket_entry(item, product)
      basket << BasketEntry.new(item, product[:description], product[:price])
    end

end
