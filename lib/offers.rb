module Offers

  def calc_discount(item)
    if has_offer?(item)
      - BigDecimal(offers[item]['discount'].to_s)
    else
      0
    end
  end

  private

    def has_offer?(item)
      return false unless offers[item]

      if cnt_rule = offers[item]['condition']['count']
        item_entries = basket.select { |entry| entry['product_id'] == item }
        item_entries.size % cnt_rule == 0
      else
        false
      end
    end

    def offers
      @offers ||= YAML::load_file('config/seeds/offers.yml')['offers']
    end
end
