require 'spec_helper'
require 'yaml'
require 'basket.rb'

RSpec.describe Basket do
  let(:catalog) { YAML::load_file('config/seeds/products.yml')['products'] }
  let(:offers) { YAML::load_file('config/seeds/offers.yml')['offers'] }

  subject { described_class.new(catalog: catalog, offers: offers) }

  describe "#add" do
    it 'increments the number of products in the basket' do
      expect { subject.add('J01') }.to change { subject.basket.count }.by(1)
      expect { subject.add('S01') }.to change { subject.basket.count }.by(1)
      expect { subject.add('XYZ') }.to raise_error 'key not found: "XYZ"'
    end
  end

  describe '#subtotal' do
    it 'no offer applied' do
      subject.add 'J01'
      subject.add 'B01'
      expect(subject.subtotal).to eq(57.9)
    end

    it "applies half price to the 2nd pair of jeans" do
      subject.add 'J01'
      subject.add 'J01'
      expect(subject.subtotal).to eq(49.42)
    end
  end

  describe '#total' do
    it {
      subject.add 'S01'
      subject.add 'B01'
      expect(subject.total).to eq(37.85)
    }

    it {
      subject.add 'J01'
      subject.add 'J01'
      expect(subject.total).to eq(54.37)
    }

    it {
      subject.add 'J01'
      subject.add 'B01'
      expect(subject.total).to eq(60.85)
    }

    it {
      subject.add 'S01'
      subject.add 'S01'
      subject.add 'J01'
      subject.add 'J01'
      subject.add 'J01'
      expect(subject.total).to eq(98.27)
    }
  end
end
